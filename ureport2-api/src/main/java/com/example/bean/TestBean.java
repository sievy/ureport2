package com.example.bean;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author anchao
 */
@Component("testBean")
public class TestBean {

    public List<Map<String,Object>> loadReportData(String dsName, String datasetName, Map<String,Object> parameters){
        System.out.println(String.format("dsName:%s datasetName:%s parameters:%s",dsName,datasetName,parameters));
        return null;
    }
    public List<User> buildReport(String dsName,String datasetName,Map<String,Object> parameters){
        System.out.printf("dsName:%s datasetName:%s parameters:%s%n",dsName,datasetName,parameters);
        User user;
        List<User> list = new ArrayList<>();
        for (int i = 0; i <100 ; i++) {
            user = new User();
            user.setId(i);
            user.setName("testName"+i);
            list.add(user);
        }
        return list;
    }
}
