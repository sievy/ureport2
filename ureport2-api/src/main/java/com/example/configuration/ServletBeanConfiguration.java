package com.example.configuration;

import com.bstek.ureport.UReportPropertyPlaceholderConfigurer;
import com.bstek.ureport.console.UReportServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.io.ClassPathResource;

/**
 * @author anchao
 */
@Configuration
@ImportResource("classpath:ureport-console-context.xml")
public class ServletBeanConfiguration {

    /**
     * 注册Servlet
     */
    @Bean
    public ServletRegistrationBean buildUReportServlet(){
        return new ServletRegistrationBean(new UReportServlet(),"/ureport/*");
    }

    /**
     * 读取配置
     */
    @Bean
    public UReportPropertyPlaceholderConfigurer propertyConfigurer(){
        UReportPropertyPlaceholderConfigurer uReportPropertyPlaceholderConfigurer = new UReportPropertyPlaceholderConfigurer();
        uReportPropertyPlaceholderConfigurer.setLocation(new ClassPathResource("config.properties"));
        uReportPropertyPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
        return uReportPropertyPlaceholderConfigurer;
    }
}
