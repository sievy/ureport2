package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ureport2ApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ureport2ApiApplication.class, args);
    }

}
