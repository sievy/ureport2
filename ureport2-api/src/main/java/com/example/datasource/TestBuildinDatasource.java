package com.example.datasource;

import com.bstek.ureport.definition.datasource.BuildinDatasource;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author anchao
 */
@Component
public class TestBuildinDatasource implements BuildinDatasource {
    @Resource
    private DataSource dataSource;
    @Override
    public String name() {
        return "内置数据源01";
    }

    @Override
    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
